# 'algorithms.py'
# Author: Wenjun Wang
# Last Updated: 11/11/2019


# This script contains all of the pathfinding algorithms in this project as well as some helper functions.

from guppy import hpy
h = hpy()

display_mem = True


def dfs(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    connections = {test.start_vertex(): None}
    fringe = []
    last_node = None
    for neighbor, _ in test.neighbors(test.start_vertex()):
        fringe.append((neighbor, test.start_vertex()))
    while len(fringe) > 0:
        vertex, parent = fringe.pop()
        connections[vertex] = parent
        if test.is_end_vertex(vertex):
            last_node = vertex
            break
        for neighbor, _ in test.neighbors(vertex):
            for n, _ in fringe:
                if n == neighbor:
                    fringe.remove((n, _))
            if neighbor in connections:
                if display_mem:
                    print(h.heap().size)
                return
            fringe.append((neighbor, vertex))
    if last_node is None:
        if display_mem:
            print(h.heap().size)
        return
    solution = [last_node]
    while not last_node == test.start_vertex():
        last_node = connections[last_node]
        solution.insert(0, last_node)
    if display_mem:
        print(h.heap().size)
    return solution


def bfs(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    connections = {test.start_vertex(): None}
    fringe = []
    last_node = None
    for neighbor, _ in test.neighbors(test.start_vertex()):
        fringe.insert(0, (neighbor, test.start_vertex()))
    while len(fringe) > 0:
        vertex, parent = fringe.pop()
        connections[vertex] = parent
        if test.is_end_vertex(vertex):
            last_node = vertex
            break
        for neighbor, _ in test.neighbors(vertex):
            if neighbor not in connections:
                valid_node = True
                for fringe_node, _ in fringe:
                    if fringe_node == neighbor:
                        valid_node = False
                if valid_node:
                    fringe.insert(0, (neighbor, vertex))
    if last_node is None:
        if display_mem:
            print(h.heap().size)
        return
    solution = [last_node]
    while not last_node == test.start_vertex():
        last_node = connections[last_node]
        solution.insert(0, last_node)
    if display_mem:
        print(h.heap().size)
    return solution


def ucs(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    connections = {test.start_vertex(): None}
    fringe = []
    last_node = None
    goal_cost = float('inf')
    for neighbor, cost in test.neighbors(test.start_vertex()):
        fringe.append((cost, neighbor, test.start_vertex()))
    fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    while len(fringe) > 0:
        cost, vertex, parent = fringe.pop()
        if cost >= goal_cost:
            break
        connections[vertex] = parent
        if test.is_end_vertex(vertex) and cost < goal_cost:
            last_node = vertex
            goal_cost = cost
            continue
        for neighbor, neighbor_cost in test.neighbors(vertex):
            if neighbor not in connections:
                valid_node = True
                for fringe_cost, fringe_node, _ in fringe:
                    if fringe_node == neighbor:
                        if cost + neighbor_cost < fringe_cost:
                            fringe.remove((fringe_cost, fringe_node, _))
                        else:
                            valid_node = False
                if valid_node:
                    fringe.append((cost + neighbor_cost, neighbor, vertex))
        fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    if last_node is None:
        if display_mem:
            print(h.heap().size)
        return
    solution = [last_node]
    while not last_node == test.start_vertex():
        last_node = connections[last_node]
        solution.insert(0, last_node)
    if display_mem:
        print(h.heap().size)
    return solution


def null_heuristic():
    return 0


def arithmetic_difference_heuristic(current, end):
    return abs(end - current)


def astar_null_heuristic(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    connections = {test.start_vertex(): None}
    fringe = []
    last_node = None
    goal_cost = float('inf')
    for neighbor, cost in test.neighbors(test.start_vertex()):
        fringe.append((cost + null_heuristic(), neighbor, test.start_vertex()))
    fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    while len(fringe) > 0:
        cost, vertex, parent = fringe.pop()
        if cost >= goal_cost:
            break
        connections[vertex] = parent
        if test.is_end_vertex(vertex) and cost < goal_cost:
            last_node = vertex
            goal_cost = cost
            continue
        for neighbor, neighbor_cost in test.neighbors(vertex):
            if neighbor not in connections:
                valid_node = True
                for fringe_cost, fringe_node, _ in fringe:
                    if fringe_node == neighbor:
                        if cost + neighbor_cost < fringe_cost:
                            fringe.remove((fringe_cost, fringe_node, _))
                        else:
                            valid_node = False
                if valid_node:
                    fringe.append((cost + neighbor_cost + null_heuristic(), neighbor, vertex))
        fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    if last_node is None:
        if display_mem:
            print(h.heap().size)
        return
    solution = [last_node]
    while not last_node == test.start_vertex():
        last_node = connections[last_node]
        solution.insert(0, last_node)
    if display_mem:
        print(h.heap().size)
    return solution


def astar_arithmetic_difference_heuristic(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    connections = {test.start_vertex(): None}
    fringe = []
    last_node = None
    goal_node = test.end_vertex()
    goal_cost = float('inf')
    for neighbor, cost in test.neighbors(test.start_vertex()):
        fringe.append((cost + arithmetic_difference_heuristic(neighbor, goal_node), neighbor, test.start_vertex()))
    fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    while len(fringe) > 0:
        cost, vertex, parent = fringe.pop()
        if cost >= goal_cost:
            break
        connections[vertex] = parent
        if test.is_end_vertex(vertex) and cost < goal_cost:
            last_node = vertex
            goal_cost = cost
            continue
        for neighbor, neighbor_cost in test.neighbors(vertex):
            if neighbor not in connections:
                valid_node = True
                for fringe_cost, fringe_node, _ in fringe:
                    if fringe_node == neighbor:
                        if cost + neighbor_cost < fringe_cost:
                            fringe.remove((fringe_cost, fringe_node, _))
                        else:
                            valid_node = False
                if valid_node:
                    fringe.append((cost + neighbor_cost + arithmetic_difference_heuristic(neighbor, goal_node), neighbor, vertex))
        fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
    if last_node is None:
        if display_mem:
            print(h.heap().size)
        return
    solution = [last_node]
    while not last_node == test.start_vertex():
        last_node = connections[last_node]
        solution.insert(0, last_node)
    if display_mem:
        print(h.heap().size)
    return solution


def WAD(test):
    if display_mem:
        h.setrelheap()
    if test.is_end_vertex(test.start_vertex()):
        if display_mem:
            print(h.heap().size)
        return test.start_vertex(),
    solution = [test.start_vertex()]
    visited = []
    while True:
        min_neighbor = None
        min_neighbor_cost = float('inf')
        visited.append(solution[len(solution)-1])
        for neighbor, cost in test.neighbors(solution[len(solution)-1]):
            if test.is_end_vertex(neighbor):
                solution.append(neighbor)
                if display_mem:
                    print(h.heap().size)
                return solution
            if cost < min_neighbor_cost:
                min_neighbor = neighbor
                min_neighbor_cost = cost
        if min_neighbor in visited:
            if display_mem:
                print(h.heap().size)
            return
        solution.append(min_neighbor)
