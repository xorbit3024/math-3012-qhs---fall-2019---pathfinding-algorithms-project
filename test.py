# 'test.py'
# Author: Wenjun Wang
# Last Updated: 11/11/2019


# This script contains the Test class which creates problems using a Graph object, a start vertex, and an end vertex.

from collections import defaultdict


class Test:
    def __init__(self, graph, start, end):
        self.__graph = graph
        if start in self.__graph.vertices:
            self.__start_vertex = start
        else:
            raise Exception('start vertex not in graph')
        if end in self.__graph.vertices:
            self.__end_vertex = end
        else:
            raise Exception('end vertex not in graph')
        self.__neighbors = defaultdict(list)
        for vertex in self.__graph.vertices:
            for edge in self.__graph.edges:
                if self.__graph.undirected:
                    if vertex == edge[0][0]:
                        self.__neighbors[vertex].append((edge[0][1], edge[1]))
                    elif vertex == edge[0][1]:
                        self.__neighbors[vertex].append((edge[0][0], edge[1]))
                else:
                    if vertex == edge[0][0]:
                        self.__neighbors[vertex].append((edge[0][1], edge[1]))

        self.ucs_solution = self.__ucs(self)

    def start_vertex(self):
        return self.__start_vertex

    def is_end_vertex(self, test_vertex):
        return test_vertex == self.__end_vertex

    def end_vertex(self):
        return self.__end_vertex

    def neighbors(self, test_vertex):
        return self.__neighbors.get(test_vertex, [])

    def check_solution(self, solution):
        if solution is None:
            return True if self.ucs_solution is None else False

        if solution[0] == self.__start_vertex and solution[len(solution)-1] == self.__end_vertex:
            for index in range(0, len(solution) - 1):
                connected = False
                for neighbor in self.__neighbors[solution[index]]:
                    if solution[index+1] == neighbor[0]:
                        connected = True
                        break
                if not connected:
                    return False
            return True
        else:
            return False

    def check_optimal_solution(self, solution):
        if solution is None:
            return True if self.ucs_solution is None else False

        if solution[0] == self.__start_vertex and solution[len(solution) - 1] == self.__end_vertex:
            return True if self.__cost_of(solution) == self.__cost_of(self.ucs_solution) else False
        else:
            return False

    def __cost_of(self, path):
        cost = 0
        for index in range(0, len(path) - 1):
            for neighbor in self.__neighbors[path[index]]:
                if path[index + 1] == neighbor[0]:
                    cost += neighbor[1]
        return cost

    @staticmethod
    def __ucs(test):
        if test.is_end_vertex(test.start_vertex()):
            return test.start_vertex(),
        connections = {test.start_vertex(): None}
        fringe = []
        last_node = None
        goal_cost = float('inf')
        for neighbor, cost in test.neighbors(test.start_vertex()):
            fringe.append((cost, neighbor, test.start_vertex()))
        fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
        while len(fringe) > 0:
            cost, vertex, parent = fringe.pop()
            if cost >= goal_cost:
                break
            connections[vertex] = parent
            if test.is_end_vertex(vertex) and cost < goal_cost:
                last_node = vertex
                goal_cost = cost
                continue
            for neighbor, neighbor_cost in test.neighbors(vertex):
                if neighbor not in connections:
                    valid_node = True
                    for fringe_cost, fringe_node, _ in fringe:
                        if fringe_node == neighbor:
                            if cost + neighbor_cost < fringe_cost:
                                fringe.remove((fringe_cost, fringe_node, _))
                            else:
                                valid_node = False
                    if valid_node:
                        fringe.append((cost + neighbor_cost, neighbor, vertex))
            fringe = sorted(fringe, key=lambda i: i[0], reverse=True)
        if last_node is None:
            return
        solution = [last_node]
        while not last_node == test.start_vertex():
            last_node = connections[last_node]
            solution.insert(0, last_node)
        return solution
