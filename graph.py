# 'graph.py'
# Author: Wenjun Wang
# Last Updated: 11/11/2019


# This script contains the graph class which reads text files into a Graph object

class Graph:
    def __init__(self, filename):
        with open(filename, 'r') as file:
            self.__name = file.readline().rstrip()
            if file.readline().rstrip() == 'u':
                self.undirected = True
            else:
                self.undirected = False
            self.vertices = range(0, int(file.readline().rstrip()))
            self.edges = []
            edge = file.readline()
            while edge != '':
                edge = edge.rstrip().split(' ')
                self.edges.append(((int(edge[0]), int(edge[1])), int(edge[2])))
                edge = file.readline()
