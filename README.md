# Pathfinding Algorithms Project

#### Authors
Wenjun Wang

Rohit Arunachalam

Gautam Desai

#### Language
Python 3.7.5

#### Setup
1) Install Python 3.7.5 if you do not already have it installed. Link here:
https://www.python.org/downloads/release/python-375/

2) Install guppy3 if you do not already have it installed. Link here:
https://pypi.org/project/guppy3/


3) Clone the project anywhere you like.

4) Open 'test_program.py' with your favorite text editor and follow the directions
inside.

5) Run 'test_program.py' with Python 3.7.5 to perform the desired
test(s).