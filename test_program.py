# 'test_program.py'
# Author: Wenjun Wang
# Last Updated: 11/11/2019


# This head script contains the functions to test all of the search algorithms in this project.
# To perform the tests, follow the instructions in main() below. The definition starts on line 28.

from graph import Graph
from test import Test
import algorithms
from time import perf_counter_ns as now

# name of graph file for all tests
# the three graphs available for testing are:
# 1) 'small_graph.txt'
# 2) 'medium_graph.txt'
# 3) 'large_graph.txt'
graph_name = 'small_graph.txt'

# start and end vertices for  single_test()
start_vertex = 0
end_vertex = 29
# pathfinding algorithm to test with single_test()
algorithm = algorithms.astar_arithmetic_difference_heuristic


def main():
    # The three available tests are time_test(), mem_test(), and single_test()
    #
    # time_test() will measure the actual time taken by each algorithm in the project. It will ask the algorithm
    # to find a path from node 0 to each other node in the graph 3 times. All of the times will be recorded in seconds.
    # time_test() also will display two boolean values after each time. These correspond to whether or not the solution
    # reported by the algorithm is a valid solution and whether or not it is an optimal solution, respectively.
    #
    # mem_test() will measure the memory taken in bytes by each algorithm in the project. It will ask the algorithm
    # to find a path from node 0 to each other node in the graph 3 times. All of the memories taken will be recorded
    # in bytes.
    #
    # single_test() will measure the actual time taken and memory taken by a single algorithm. It will ask the algorithm
    # to find a path from start_vertex to end_vertex once. The time will be reported in seconds. The memory will be
    # reported in bytes.
    #
    # To select a test (or multiple tests), modify the code in this main() function to call the test desired.

    mem_test()


def time_test():
    g = Graph(graph_name)
    algorithms.display_mem = False
    test_start = 0
    print('DFS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.dfs(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution), sep='\t')
    print('\nBFS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.bfs(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution), sep='\t')
    print('\nUCS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.ucs(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution),
                  sep='\t')
    print('\nA*null')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.astar_null_heuristic(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution), sep='\t')
    print('\nA*diff')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.astar_arithmetic_difference_heuristic(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution), sep='\t')
    print('\nWAD')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            start_time = now()
            solution = algorithms.WAD(t)
            end_time = now()
            print((end_time - start_time) / 1e9, t.check_solution(solution), t.check_optimal_solution(solution), sep='\t')
    input('Enter anything to quit.')


def mem_test():
    g = Graph(graph_name)
    algorithms.display_mem = True
    test_start = 0
    print('DFS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.dfs(t)
    print('\nBFS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.bfs(t)
    print('\nUCS')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.ucs(t)
    print('\nA*null')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.astar_null_heuristic(t)
    print('\nA*diff')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.astar_arithmetic_difference_heuristic(t)
    print('\nWAD')
    for test_end in range(0, len(g.vertices)):
        for i in range(0, 3):
            t = Test(g, test_start, test_end)
            algorithms.WAD(t)
    input('Enter anything to quit.')


def single_test():
    g = Graph(graph_name)
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST REPORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('')
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PROBLEM PROPERTIES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('Graph Size:', len(g.vertices), 'vertices,', len(g.edges), 'edges')
    print('Start Vertex:', start_vertex)
    print('End Vertex:', end_vertex)
    print('')
    t = Test(g, start_vertex, end_vertex)
    algorithms.display_mem = True
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SOLUTION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('Memory Taken (bytes): ', end='')
    start_time = now()
    solution = algorithm(t)
    end_time = now()
    print('Time Elapsed (s):', (end_time - start_time) / 1e9)
    print('Correct Solution?', t.check_solution(solution))
    print('Optimal Solution?', t.check_optimal_solution(solution))
    if solution is not None:
        print('Solution:\t\t\t\t\t\t', tuple(solution))
    else:
        print('Solution:\t\t\t\t\t\t', 'None')
    if t.ucs_solution is not None:
        print('Correct and Optimal Solution:\t', tuple(t.ucs_solution))
    else:
        print('Correct and Optimal Solution:\t', 'None')
    input('Enter anything to quit.')


if __name__ == '__main__':
    main()
